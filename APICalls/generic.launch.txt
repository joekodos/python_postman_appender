		{
			"id": "itemIDnum",
			"headers": "Authorization: IPC {{auth-dca}}\nININ-Session: {{sess-id}}\nContent-Type: application/json\n",
			"url": "{{server_url_dca}}/api/v1/processautomation/flows/instances",
			"preRequestScript": "",
			"pathVariables": {},
			"method": "POST",
			"data": [],
			"dataMode": "raw",
			"version": 2,
			"tests": "if (responseCode.code >= 200 && responseCode.code <= 299) {\r\n        tests[\"NULL Launch Flow\"] = responseCode.code === 200;\r\n        var data = JSON.parse(responseBody);\r\n        postman.setEnvironmentVariable(\"instance_id\", data.id);\r\n}\r\nelse {\r\n        if (responseCode.code >= 400 && responseCode.code <= 499) {\r\n                tests[\"Response Code is Incorrect Range!\"] = false;\r\n        }\r\n        if (responseCode.code >= 500 && responseCode.code <= 599) {\r\n                tests[\"Response Code is Incorrect Range!\"] = false;\r\n        }\r\n        if (responseCode.code >= 600 || responseCode.code < 200 ) {\r\n                tests[\"Response Code is Incorrect Range!\"] = false;\r\n        }\r\n}\r\nfunction pausecomp(millis) {\r\n    var date = new Date();\r\n    var curDate = null;\r\n    do { curDate = new Date(); }\r\n    while(curDate-date < millis);\r\n}\r\npausecomp(environment.sleep_interval);",
			"currentHelper": "normal",
			"helperAttributes": {},
			"time": 1436539873621,
			"name": "Launch Flow",
			"description": "",
			"collectionId": "00000000-aaaa-0000-0000-0000000000collectionNumber",
			"responses": [],
			"synced": false,
			"rawModeData": "{\r\n  \"flowConfigId\": \"{{flow-id}}\",\r\n  \"inputData\": {    \"linkedDocumentIds\": [ \"{{linked_document_a}}\" ]  },\r\n  \"launchType\": \"NORMAL\"\r\n}"
		},