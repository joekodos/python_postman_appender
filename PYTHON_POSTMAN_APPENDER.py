#!/usr/bin/python

# Business Link Layer (bll) & Data Access Layer (dal) Created by Alex Hernandez
# Appender Created by Joseph Kodos
# Ported from APPEND_SERIES.ps1
# 11/03/2015
""" This script will read a test series and construct a postman collection
"""
# Import all necessary modules.
#import xlrd
#from collections import OrderedDict
#import simplejson as json
#import shutil
#from openpyxl import load_workbook
import bll
import os
import sys
# Title
print "\n"
print "                                             |---- PYTHON POSTMAN APPENDER v1.0 ----|"
print "\n"

"""Check Paths"""
# Check path config
bll.db.checkPathConfig()

# Set paths
apiDir = bll.db.setApiPoolDir()
testSeriesDir = bll.db.setTestSeriesDir()
testSeriesFile = bll.db.setTestSeriesFile()
headerAdir = bll.db.setHeaderADir()
headerBdir = bll.db.setHeaderBDir()
footerAdir = bll.db.setFooterADir()


#apiDir = raw_input(" > What is your API pool directory (e.g. POSTMAN_SERIES_APPENDER\APICalls): ")
#apiDirX = "\\"
#apiDir = "U:\PostmanScripts\PYTHON_POSTMAN_APPENDER\APICalls" + apiDirX
#testSeriesDir = raw_input(" > What is your test_series directory: ")
#testSeriesDir = "U:\PostmanScripts\PYTHON_POSTMAN_APPENDER"
#testSeriesFile = raw_input(" > What is your test_series file name: ")
#testSeriesFile = "test_series.txt"
#headerAdir = "U:\PostmanScripts\PYTHON_POSTMAN_APPENDER\headerA.txt"
#headerBdir = "U:\PostmanScripts\PYTHON_POSTMAN_APPENDER\headerB.txt"
#footerAdir = "U:\PostmanScripts\PYTHON_POSTMAN_APPENDER" + "\\" + "footerA.txt"

apiPoolDirList = os.listdir(apiDir)
apiPoolCount = len(apiPoolDirList)
collectionName = raw_input(" > Enter name of collection: ")
collectionNumber = raw_input(" > Enter number of collection (must be two digits): ")
#append diretory to path
testSeriesPath = testSeriesDir + testSeriesFile
testSeriesList = os.listdir(testSeriesDir)
testSeriesCount = len(testSeriesList)

#Load lines of file into array testSeriesRows
with open(testSeriesFile) as testSeriesFileX:
        testSeriesRows = testSeriesFileX.readlines()

#Get rid of extra CR LF ,except for last line
for eachItem in range(0, len(testSeriesRows)):
        if testSeriesRows[eachItem] != testSeriesRows[len(testSeriesRows) - 1] :
                testSeriesRows[eachItem] = testSeriesRows[eachItem][:-1]


headerA = open(headerAdir).read()
header0 = str(headerA)
header0a = header0.replace("collectionName", collectionName)
header1 = header0a.replace("collectionNumber", collectionNumber)
headerB = open(headerBdir).read()
header2 = str(headerB)
footerA = open(footerAdir).read()
footer1 = str(footerA)

idStringA = "00000000-a"
idStringC = "a-0000-0000-0000000000"
idStringD = ","
idStringF = "                "
idStringG = "\""
idCounter = 0
tempCounter = 0
# List to hold itemOrderAppended
itemOrderAppended = []
# List to hold appendedBody
appendedBody = []

for eachTestSeriesRow in range(0, len(testSeriesRows)):
        idCounter = idCounter + 1
        if idCounter < 10 :
                idStringB = "0" + str(idCounter)
        
        if idCounter >= 10 :
                idStringB = str(idCounter)
        
        #if counter reaches end, don't put a comma there
        if idCounter == len(testSeriesRows) :
                idStringAll = idStringF + idStringG + idStringA + collectionNumber + idStringC + idStringB + idStringG
        
        #else, put a comma there
        if idCounter != len(testSeriesRows) :
                idStringAll = idStringF + idStringG + idStringA + collectionNumber + idStringC + idStringB + idStringG + idStringD
        
        idCounter = idCounter - 1
        #idStringB is the sequence number
        #idStringAll = "00000000-a" + 15 + a-0000-0000-0000000000 + 05
        itemOrderAppended.append(idStringAll)
        itemOrderAppendedJoined = " ".join(itemOrderAppended)
        
        # Find matching file (i.e. "generic.publish" in the file --> grab content of generic.publish.txt )
        for eachAPICallInPool in range(0, apiPoolCount):
                currentFileNameWithExtension = apiPoolDirList[eachAPICallInPool]
                currentFile = currentFileNameWithExtension[:-4]
                #print "testSeriesRows[idCounter]"
                #print testSeriesRows[idCounter]
                #print currentFile
                #print "testSeriesRows[idCounter]"
                if currentFile == testSeriesRows[idCounter]:
                        # do stuff here that grabs that matching file and copies its content to a body file
                        currentCallData = open(apiDir + currentFileNameWithExtension).read()
                        idStringCurrentItem = idStringA + collectionNumber + idStringC + idStringB
                        #update current API item ID and collection ID fields
                        currentUpdatedCallData = currentCallData.replace("itemIDnum", idStringCurrentItem)
                        currentFinishedCallData = currentUpdatedCallData.replace("collectionNumber", collectionNumber)
                        appendedBody.append(currentFinishedCallData)
                        appendedBodyJoined = " ".join(appendedBody)
        
        idCounter = idCounter + 1

#create new collection file
collectionFile = open(collectionName + ".postman_collection", 'w')
#append item order to main file
collectionFile.write(header1)
collectionFile.write(str(itemOrderAppendedJoined))
collectionFile.write(header2)
collectionFile.write(str(appendedBodyJoined))
collectionFile.write(footer1)


print "\n"
print "---------------------------- APPENDER OPERATION COMPLETED SUCCESSFULLY ---------------------------------"
print "\n"
print "         \/!\\    \/!\\     \/!\\     \/!\\     \/!\\     \/!\\     \/!\\              "
print "                      "
print "Please remove the last comma in your new postman_collection file before importing !!!!"
print "                      "
print "         \/!\\    \/!\\     \/!\\     \/!\\     \/!\\     \/!\\     \/!\\              "
print "\n"