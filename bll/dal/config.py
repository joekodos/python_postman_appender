import ConfigParser
import os

def checkMacConfig():
        # create config file and check if it is null
        config = ConfigParser.ConfigParser()
        
        config.read("config.ini")
        macPath = config.get("Paths", "apiPoolDir")
        
        if macPath == "":
                askAPIpoolDir = raw_input("Set a path for the API calls directory. (Ex: /testing/apiCalls/): ")
                print "\n"
                askTestSeriesDir = raw_input("Set a path for the directory the test series will reside in. (Ex: /testing/) ")
                print "\n"
                askTestSeriesFile = raw_input("Set the test series filename. (Ex: test_series.txt) ")
                print "\n"
                askHeaderAdir = raw_input("Set the path for headerA file. (Ex:/testing/headerA.txt) ")
                print "\n"
                askHeaderBdir = raw_input("Set the path for headerB file. (Ex:/testing/headerB.txt) ")
                print "\n"
                askFooterAdir = raw_input("Set the path for footerA file. (Ex:/testing/footerA.txt) ")
                print "\n"

                file1 = open("config.ini", "w")
                config.set("Paths", "apiPoolDir", askAPIpoolDir)
                config.set("Paths", "testSeriesDir", askTestSeriesDir)
                config.set("Paths", "testSeriesFile", askTestSeriesFile)
                config.set("Paths", "headerADir", askHeaderAdir)
                config.set("Paths", "headerBDir", askHeaderBdir)
                config.set("Paths", "footerADir", askFooterAdir)
                config.write(file1)
                file1.close()
        
        config.read("config.ini")
        
        apiPoolDir = config.get("Paths", "apiPoolDir")
        testSeriesDir = config.get("Paths", "testSeriesDir")
        testSeriesFile = config.get("Paths", "testSeriesFile")
        headerADir = config.get("Paths", "askHeaderAdir")
        headerBDir = config.get("Paths", "askHeaderBdir")
        footerADir = config.get("Paths", "askFooterAdir")
        
        print "Your API pool directory is: \n" + apiPoolDir
        print "\n"
        print "Your Test Series directory is: \n" + testSeriesDir
        print "\n"
        print "Your Test Series File is called: \n" + testSeriesFile
        print "\n"
        print "Your headerA file is: \n" + headerADir
        print "\n"
        print "Your headerB file is: \n" + headerBDir
        print "\n"
        print "Your footerA file is: \n" + footerADir
        print "\n"
        
        pathQuestion = raw_input("Is this correct: (Y/N) ").lower()
        while pathQuestion == "n":
                askAPIpoolDir = raw_input("Set a path to the API calls directory. (Ex: /testing/apiCalls/): ")
                print "\n"
                askTestSeriesDir = raw_input("Set a path to your test series directory. (Ex: /testing/) ")
                print "\n"
                askTestSeriesFile = raw_input("Set your test series file name. (Ex: /testing/test_series.txt) ")
                print "\n"
                askHeaderAdir = raw_input("Set a path to your headerA file. (Ex:/testing/headerA.txt) ")
                print "\n"
                askHeaderBdir = raw_input("Set a path to your headerB file. (Ex:/testing/headerB.txt) ")
                print "\n"
                askFooterAdir = raw_input("Set a path to your footerA file. (Ex:/testing/footerA.txt) ")
                print "\n"
                
                file1 = open("config.ini", "w")
                config.set("Paths", "apiPoolDir", askAPIpoolDir)
                config.set("Paths", "testSeriesDir", askTestSeriesDir)
                config.set("Paths", "testSeriesFile", askTestSeriesFile)
                config.set("Paths", "headerADir", askHeaderAdir)
                config.set("Paths", "headerBDir", askHeaderBdir)
                config.set("Paths", "footerADir", askFooterAdir)
                config.write(file1)
                file1.close()
                
                apiPoolDir = config.get("Paths", "apiPoolDir")
                testSeriesDir = config.get("Paths", "testSeriesDir")
                testSeriesFile = config.get("Paths", "testSeriesFile")
                headerADir = config.get("Paths", "headerADir")
                headerBDir = config.get("Paths", "headerBDir")
                footerADir = config.get("Paths", "footerADir")
                
                print "Your API pool directory is: \n" + apiPoolDir
                print "\n"
                print "Your Test Series directory is: \n" + testSeriesDir
                print "\n"
                print "Your Test Series File is called: \n" + testSeriesFile
                print "\n"
                print "Your headerA file is: \n" + headerADir
                print "\n"
                print "Your headerB file is: \n" + headerBDir
                print "\n"
                print "Your footerA file is: \n" + footerADir
                print "\n"
                
                pathQuestion = raw_input("Is this correct?: (Y/N) ")
                if not pathQuestion == "n":
                        break
        return

def checkWindowsConfig():
        # create config file and check if it is null
        config = ConfigParser.ConfigParser()

        config.read("config.ini")
        windowsPath = config.get("Paths", "apiPoolDir")

        if windowsPath == "":
                askAPIpoolDir = raw_input("Set a path to the API calls directory. (Ex: C:\gtesting\apiCalls\): ")
                print "\n"
                askTestSeriesDir = raw_input("Set a path to your test series directory. (Ex: C:\gtesting\) ")
                print "\n"
                askTestSeriesFile = raw_input("Set your test series file name. (Ex: C:\gtesting\test_series.txt) ")
                print "\n"
                askHeaderAdir = raw_input("Set a path to your headerA file. (Ex: C:\gtesting\headerA.txt) ")
                print "\n"
                askHeaderBdir = raw_input("Set a path to your headerB file. (Ex: C:\gtesting\headerB.txt) ")
                print "\n"
                askFooterAdir = raw_input("Set a path to your footerA file. (Ex: C:\gtesting\footerA.txt) ")
                print "\n"

                file1 = open("config.ini", "w")
                config.set("Paths", "apiPoolDir", askAPIpoolDir)
                config.set("Paths", "testSeriesDir", askTestSeriesDir)
                config.set("Paths", "testSeriesFile", askTestSeriesFile)
                config.set("Paths", "headerADir", askHeaderAdir)
                config.set("Paths", "headerBDir", askHeaderBdir)
                config.set("Paths", "footerADir", askFooterAdir)
                config.write(file1)
                file1.close()

        config.read("config.ini")

        apiPoolDir = config.get("Paths", "apiPoolDir")
        testSeriesDir = config.get("Paths", "testSeriesDir")
        testSeriesFile = config.get("Paths", "testSeriesFile")
        headerADir = config.get("Paths", "headerADir")
        headerBDir = config.get("Paths", "headerBDir")
        footerADir = config.get("Paths", "footerADir")

        print "Your API pool directory is: \n" + apiPoolDir
        print "\n"
        print "Your Test Series directory is: \n" + testSeriesDir
        print "\n"
        print "Your Test Series File is called: \n" + testSeriesFile
        print "\n"
        print "Your headerA file is: \n" + headerADir
        print "\n"
        print "Your headerB file is: \n" + headerBDir
        print "\n"
        print "Your footerA file is: \n" + footerADir
        print "\n"



        pathQuestion = raw_input("Is this correct: (Y/N) \n").lower()
        while pathQuestion == "n":
                askAPIpoolDir = raw_input("Set a path to the API calls directory. (Ex: C:\\testing\\apiCalls\): ")
                print "\n"
                askTestSeriesDir = raw_input("Set a path to your test series directory. (Ex: C:\\testing\) ")
                print "\n"
                askTestSeriesFile = raw_input("Set your test series file name. (Ex: C:\\testing\\test_series.txt) ")
                print "\n"
                askHeaderAdir = raw_input("Set a path to your headerA file. (Ex: C:\\testing\\headerA.txt) ")
                print "\n"
                askHeaderBdir = raw_input("Set a path to your headerB file. (Ex: C:\\testing\\headerB.txt) ")
                print "\n"
                askFooterAdir = raw_input("Set a path to your footerA file. (Ex: C:\\testing\\footerA.txt) ")
                print "\n"

                file1 = open("config.ini", "w")
                config.set("Paths", "apiPoolDir", askAPIpoolDir)
                config.set("Paths", "testSeriesDir", askTestSeriesDir)
                config.set("Paths", "testSeriesFile", askTestSeriesFile)
                config.set("Paths", "headerADir", askHeaderAdir)
                config.set("Paths", "headerBDir", askHeaderBdir)
                config.set("Paths", "footerADir", askFooterAdir)
                config.write(file1)
                file1.close()

                apiPoolDir = config.get("Paths", "apiPoolDir")
                testSeriesDir = config.get("Paths", "testSeriesDir")
                testSeriesFile = config.get("Paths", "testSeriesFile")
                headerADir = config.get("Paths", "headerADir")
                headerBDir = config.get("Paths", "headerBDir")
                footerADir = config.get("Paths", "footerADir")

                print "Your API pool directory is: \n" + apiPoolDir
                print "\n"
                print "Your Test Series directory is: \n" + testSeriesDir
                print "\n"
                print "Your Test Series File is called: \n" + testSeriesFile
                print "\n"
                print "Your headerA file is: \n" + headerADir
                print "\n"
                print "Your headerB file is: \n" + headerBDir
                print "\n"
                print "Your footerA file is: \n" + footerADir
                print "\n"

                pathQuestion = raw_input("Is this correct?: (Y/N) ")
                if not pathQuestion == "n":
                        break
        return


def getApiPoolDir():
    # create config file and check if it is null
    config = ConfigParser.ConfigParser()
    config.read("config.ini")
    path = config.get("Paths", "apiPoolDir")
    return path
    
def getTestSeriesDir():
    # create config file and check if it is null
    config = ConfigParser.ConfigParser()
    config.read("config.ini")
    path = config.get("Paths", "testSeriesDir")
    return path

def getTestSeriesFile():
    # create config file and check if it is null
    config = ConfigParser.ConfigParser()
    config.read("config.ini")
    path = config.get("Paths", "testSeriesFile")
    return path
    
def getHeaderADir():
    # create config file and check if it is null
    config = ConfigParser.ConfigParser()
    config.read("config.ini")
    path = config.get("Paths", "headerADir")
    return path
    
def getHeaderBDir():
    # create config file and check if it is null
    config = ConfigParser.ConfigParser()
    config.read("config.ini")
    path = config.get("Paths", "headerBDir")
    return path

def getFooterADir():
    # create config file and check if it is null
    config = ConfigParser.ConfigParser()
    config.read("config.ini")
    path = config.get("Paths", "footerADir")
    return path
